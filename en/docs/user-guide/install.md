# Installation

This page explains how to get and install Karaoke Mugené Mugen.

**NOTE :** Once installed, Karaoke Mugen does not need any internet connection, except for some features :

- Remote access for the public through the URL https://xx.kara.moe
- Online accounts
- Song updates on startup
- Most popular song lists
- Playing songs **which haven't been downloaded locally yet**

## Download Karaoke Mugen

Binaries for Windows, macOS and Linux are available. You'll find either installers or portable installs. **If you don't know which to pick, we recommend the installer version.**

[Go to the downloads page](http://mugen.karaokes.moe/en/download.html)

Extract the .zip archive in a folder of your choice where there's a lot of disk space available.

## Install Karaoke Mugen

There are several ways to install depending on if you want the app from source or from compiled versions ready to use.

Once done, you can go to the [getting started](getting-started.md) section!

### Windows

Two choices :

#### Installer

Double-click on the executable and follow instructions

#### Portable

Extract the .zip archive in a folder/drive where you have enough space available.

### macOS

#### Image DMG

Double-click on the DMG image and drag & drop Karaoke Mugen in your application folder.

Depending on your macOS version you might need to right-click the DMG image or the app and select "Open" the first time to force it to open since the system is more restrictive towards non-signed applications. We don't have the money to sign Karaoke Mugen yet.

### Linux

#### Debian/Ubuntu package (.deb)

The Debian package will install PostgreSQL, mvp and ffmpeg as dependencies to make sure Karaoke Mugen works. The package should also create an entry in your application menu.

After installing this package, you will need to configure PostgreSQL. You should take a look at your distribution's documentation on how to setup PostgreSQL.

To configure the database for Karaoke Mugen, you'll need to create it and name it `karaokemugen_app` (or any other name) as well as a role for the app.

Here are the commands you'll need to use on Debian or Ubuntu (this can vary a little depending on your distro)

```Shell
user$ sudo -iu postgres
postgres$ createuser -P karaokemugen_app
> Un mot de passe vous sera demandé. Mettez celui que vous voulez.
postgres$ createdb karaokemugen_app -O karaokemugen_app -E UTF8
postgres$ psql -c "create extension unaccent;" karaokemugen_app
postgres$ psql -c "create extension pgcrypto;" karaokemugen_app
```

After this, you will need to [configure Karaoke Mugen](advanced.md#database) to tell the app which password you used to connect via the `karaokemugen_app` user.

#### ArchLinux

There are two AUR packages, [`karaokemugen`](https://aur.archlinux.org/packages/karaokemugen) and [`karaokemugen-git`](https://aur.archlinux.org/packages/karaokemugen-git/). The first is based on the latest stable version and the second on the last commit from the `master` git branch. They both can be installed in the same way.

##### Installation

```Shell
$ pikaur -S karaokemugen-git
```
![Pikaur](../img/setup/km40.jpg)

_Installation can take some time depending on your configuration, from 10 to 20 minutes._

Once done, your console should look like this :

![Pikaur end](../img/setup/km41.jpg)

_If you see this, everything went well!_

##### Initial configuration

As you saw, the AUR package comes with the `karaokemugen-install` utility, allowing you to automatically configure Karaoke Mugen! Launch this utility as soon as install is over.

```Shell
$ karaokemugen-install
```

**Warning:** as said during launch, this script will probably not work if you changed the default PostgreSQL configuration. This script will need _super-user_ rights several times via _sudo_.

At launch, the script warns you of potential problems which could keep it from working and then does all necessary actions to make Karaoke Mugen work (create database, apply configuration, etc.)

This script is interactive. It may ask you a few questions on what to do (especially if a previous Karaoke Mugen database is found) and other configuration items.

![karaokemugen-install](../img/setup/km42.jpg)

Once done, you're ready to launch Karaoke Mugen.

##### Launch

Congratulations! Your Karaoke Mugen install is ready to start so you can experience incredible adventures.

You should be able to find Karaoke Mugen in your  Applications menu's desktop environment. If that's not the case, launch it simply with `karaokemugen` in a terminal.

![App Launch](../img/setup/km44.png)

![Karaoke Mugen](../img/setup/km43.jpg)

##### It doesn't work?

First, sorry. We want the install to be as simple as possible but we can't think of everything. Don't hesitate to [contact us](../about/contact.md) if you have a problem.

## Install from source

You can install Karaoke Mugen by downloading its source code.

- Download a Karaoke Mugen version from [its git repository](https://gitlab.com/karaokemugen/karaokemugen-app) through `git clone` or the ZIP archive provided by gitlab.
	- Each version is tagged : pick the version [you want to use](https://gitlab.com/karaokemugen/karaokemugen-app/tags).
	- If you like risks you can [download the dev version](https://gitlab.com/karaokemugen/karaokemugen-app/repository/next/archive.zip). **Beware, it most probably has bugs!**
- Unzip/place the Karaoke Mugen source code where there's enough free space.
- Download [mpv](http://mpv.io) and place its executable in the `app/bin` folder or specify its path in the [config file](configuration) if you already have mpv installed on your system.
	- Version 0.33 or later required
- Download [ffmpeg](http://ffmpeg.org) and place the `ffmpeg` executable in the  `app/bin` folder.
	- Version 3.3.1 is the minimum requirement
- Download [nodeJS](https://nodejs.org/en/download/) which is required for Karaoke Mugen to run.
	- Minimum version required is 16. Do not use a newer major version.
	- Install nodeJS once it's been downloaded
- Download [GNU Patch](https://savannah.gnu.org/projects/patch/) which is required for song database patches
    - Minimum version required is 2.7
- Download [PostgreSQL](http://postgresql.org)
    - Minimum version required is 13.x. Later versions do work.
	- You'll need to configure your database server for Karaoke Mugen to work. Modify the `config.yml` file to point to your created database. See [configuration](advanced.md#configuration). Check out the app's README to see which commands to run on the database

- Open the command line (cmd, terminal, bash...) and go into the Karaoke Mugen folder. Then launch :

If `yarn` is not installed yet on your system, go to [its website](https://yarnpkg.com) to install it.

Once `yarn` is installed :

```sh
yarn setup
```

`yarn` will install all necessary modules for Karaoke Mugen to work and build the React client. It can take from 1 to 5 minutes depending on your system's speed and your internet connection.

## Platform-specific information

### Linux

You'll need PostgreSQL installed and ready to use. [Refer to the installation from sources section](#install-karaoke-mugen-from-source)

#### Linux (Ubuntu)

The version of mpv provided by the Ubuntu repositories is obsolete, and **not compatible with Karaoke Mugen**.

You must thus install another version. [mpv's official site](https://mpv.io/) provides several ways to install Karaoke Mugen, but the simplest is for Ubuntu and its derivatives to use the following PPA :

```sh
sudo add-apt-repository ppa:mc3man/mpv-tests
sudo apt-get update
sudo apt-get install mpv
```

Check out [mpv's website](http://mpv.io) for more info.

### macOS

#### Patch

The patch version bundled with macOS is too old, you'll need to use [Homebrew](https://brew.sh) to install it.