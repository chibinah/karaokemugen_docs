
## Karaoke database

### Download songs

On its first start Karaoke Mugen will fetch all songs available in the default repository. However medias won't be downloaded right away to save on disk space.

Songs you need will be downloaded automatically when you'll add them to the current playlist.

![Download](../img/download.png)

You can also download all songs at once.

![Download_All](../img/download_all.png)

**WARNING :** Videos take at least a few hundred gigabytes, so make sure you have enough space available.

You can quit Karaoke Mugen at any time and it will resume your downloads on next start.

That said, you might not want to get everything at once, but only the songs you want. To achieve that, you can select and filter only the songs you want.

There's the search box, of course, but also a sort filter on the right, which can help you display only songs with a specific tag. For example, if you want to get all songs in a particular language, you should go to the "Tag filter" box and click on "Languages" then on the language you want so you can select one to filter songs.

![Download_Bre](../img/download_eng.png)

A simple search with your language's name would have worked as well.

Once the list displayed is the one you want, you can either download the songs of your choice or click on the button on the top right to download all songs listed.

![Download](../img/download_button.png)

### Create your own base

This is for advanced users.

You can create your own karaoke base, go to the system panel. You can add songs manually if you already have videos and .ass files of your own.

### Update your medias

* Use the system panel just like described above, simply click on "Synchronize" to fetch all updates of all configured repositories at once.
* You can also launch Karaoke Mugen via its command line with the `--updateMediasAll` option flag.

#### Repositories

Repositories are places where you can download songs. By default the OTAKU base is added to your Karaoke Mugen application, but other communities can create their own repositories which you can add so your app knows where to download new songs from.
