# Creating a karaoke

You do not find your favorite opening in the Karaoke Mugen's data and this makes you very sad? We've got a solution!

Well before everything: check twice that the song isn't already in the database.

- Search [this site](https://kara.moe) which compiles *all* of the available karaokes. Search to find out if your opening is actually in the database (or not).

- Search on the database's gitlab to see songs [about to be added](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=To%20Add) and [those being made](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=Doing).

## I double-checked, there's nothing

**If your song's not in the database, you have two options:** either you submit suggestion via [this site](https://kara.moe). Search for a song, then scroll to the bottom and use the link and fill the form to suggest us a song. Then hope that a brave soul does it for you... or you make it on your own! It's simple and we're going to explain it in that tutorial.

## Create your karaoke

**NOTE : In the following sections, you'll find advice and recommendations on video size, format, trascribing japanese and other karaoke-related things. These are RECOMMENDATIONS. You do NOT have to follow them if you are making karaoke songs for your own database.**

It's advised to follow the instructions in the given order.

- [Gather material](./material.md)
- [Make the karaoke](./karaoke.md)
- [Test](./test.md)
- [Send your karaoke](./upload.md)

## More information

- [Edit your karaoke](./editkaraoke.md)
- [Create a metadata file](./karafile.md)
- [Troubleshooting tools](./troubleshoot.md)
- [Reference files](./references.md)
- [Frequently asked questions](./faq.md)
