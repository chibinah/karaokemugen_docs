That table is indicative: for a given image resolution you should use the maximum Bitrate listed below to get an acceptable video size.

The karaoke base having a huge size already, it's important to take as little space as possible. Having a 130Mb file for 1 minute 30 of video is not acceptable.

| **Video Resolution (Y axis)** | **Maximum Birate** | **Appromative file size, for 1 min 30** |
| -------------------------------- | ---------------------- | ------------------------------------------------------ |
| 1080p | 8000 kb/s | 80Mb |
| 720p | 4000 kb/s | 40Mb |
| 480p | 3000 kb/s | 30Mb |
| 360p | 2000 kb/s | 20Mb |
