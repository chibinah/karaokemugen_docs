# Sending your karaoke

## I don't want to involve myself too much but I want to send a song.

- You can use the [import form](https://kara.moe/import).
    - That form accepts the karaoke files at the .ass format (the advised one), Ultrastar (txt), .kar, Karafun (kfn) or Epitanime Toyunda v1 to v3 (txt). For formats other than .ass, the karaoke file will be converted automatically.
    - This form will create `.kara.json` file and its associated files. All of those will be sent to our mailbox where one of the maintainers will validate and integrate the karaoke in the database. **This can take some time.**
    - An issue will also be created [there](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=ToD), allowing you to know if your karaoke file was accepted (the issue will be closed) or if there are some modifications to make (in comments).
- You can also come to our [Discord](http://karaokes.moe/discord) to send everything manually.

## I want to help and become a maintainer (and add my songs without bothering you)

That's nice of you!

After contacting us [on Discord](http://karaokes.moe/discord) we can add you as "Maintainer" on the karaoke base and help you configure your `kara.moe` repository in the app's system panel.

You'll need to [download Git](https://git-scm.com/downloads) for your operating system and install it. The default configuration should work just fine.

Now edit the `kara.moe` repository configuration (Repositories page in the system panel)

- "Maintainer mode" must be on
- These Git options must be filled in
    - `URL` : `https://gitlab.com/karaokemugen/bases/karaokebase/karaokebase.git`
    - `Username` : Your gitlab username
    - `Passowrd` : Access token for your gitlab account that you'll find in the "Preferences" > "Access Token" page on your profile. this token needs the "Write repository" permission.
    - `Git Author` : Your name as it appears on Gitlab
    - `Git Email` : Same.
    - `FTP Host` : `erin.mahoro-net.org`
    - `FTP User` : We will communicate this one to you.
    - `FTP Password` : Same here.
    - `FTP Folder` : `storage/medias-otaku`

Once your repository is configured (this can take a few minutes) you can get to the "Maintainer" menu in system panel and click on the "Git" option there. 

From this page you can "Push" your new and edited songs towards the repository in one clic. Karaoke Mugen will send your video and all the files needed to the repository.

- A series of automated tests are made by the lab to check if your modifications are okay. If it isn't the case, an error message will appear on the #git-bases channel. You can also [look at the last **pipelines**](https://gitlab.com/karaokemugen/karaokebase/pipelines) to check wether everything went well or not.
  	- If the test didn't go well, don't panic: somebody will help you seeing what the error message is about via the pipeline console (but it doesn't hurt to look!)
  	- Fix errors or ask someone else to fix it for you if you do not understand/do not have the time.
  	- Restart the pipeline or make another commit.

### About Git

Git is a versionning software. It is the best tool to manage a database like Karaoke Mugen's, because it allows to have some kind of log of who modified what and when, and to be totally sure we have exactly the same database as everyone else.

Every modification is recorded. at any time, and we can come back if we realise that we've made a mistake. More information:

- [What is Git](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F) ?
- Git clients :
    - [GitAhead](http://gitahead.com) a free tool to manage graphical git deposits.
    - [Fork](http://git-fork.com) a great tool but it is not free.

[Come on our Discord](https://karaokes.moe/discord) (in the #karaoke channel) and we will see how to add your songs. However using Git is recommended. But we realise that not everyone has the time to contribute this way.
