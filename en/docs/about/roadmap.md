# Roadmap

The app has milestones you can see here :

[Check out the Karaoke Mugen milestones](https://gitlab.com/karaokemugen/karaokemugen-app/-/milestones)

These milestones allow you to see which features are expected for a given version, and which bugs have been fixed, or are scheduled to be fixed. Sometimes, a feature or bug can be moved from one milestone to another depending on the time the team has and if the bug or feature is important or needs a lot of work or not.
