# Contact us

If you have a suggestion, a wish, or need help, there are several ways to contact us :

- Via email : `karaokemugen at shelter.moe`
- Via Twitter : [@KaraokeMugen](http://twitter.com/karaokemugen)
- Via Discord : [#karaoke channel](http://karaokes.moe/discord)

## Bugs

- If it's a bug in the app or a new feature suggestion, please [create an issue on the Karaoke Mugen app git repository](https://gitlab.com/karaokemugen/karaokemugen-app/issues/new?issue)
- If you found a badly made karaoke in our database, please [create an issue on the Karaoke Base git repository](https://gitlab.com/karaokemugen/karaokebase/issues/new?issue).

Depending on what your problem is, there are different templates you can use. You'll just have to fill in the blanks.