# License

## Software

The software (KM App, KM Server, Explorer, etc.) is under the MIT License unless said otherwise.

## Karaoke database

The databases have their own licenses depending on the type of content (data, lyrics synchronization, etc.) :

[See the OTAKU database license](https://gitlab.com/karaokemugen/karaokebase/blob/master/LICENSE.md).

## Documentation

This documentation is available under the [Creative Commons 4.0 BY-NC-SA license](http://creativecommons.org/licenses/by-nc-sa/4.0/).
