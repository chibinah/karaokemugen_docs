![Bannière Karaoke Mugen, dessin de Sedeto](./img/banner.webp)
# Welcome

Welcome to the **Karaoke Mugen** documentation.

**Karaoke Mugen** is a karaoke management software, may it be in public with a crowd or in private with friends.

- [Install](./user-guide/install.md)
- [Contribute to the karaoke database](./contrib-guide/base.md)

If you're looking for an introduction to the software and its features, checkout [the website](http://mugen.karaokes.moe)