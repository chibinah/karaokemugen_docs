# Help the karaoke team

If you aren't interested in coding, but you enjoy karaoke, managing videos or other things, we have a lot of things to do in the database department :

* [See the list of issues](https://gitlab.com/karaokemugen/karaokebase/issues) (bugs/suggestions).
* A good database is made of good data. A good database can be sorted and filtered through various criterias. This data is stored in `.kara` files. We filled a lot of those with the data we know, but there are still a vast majority of files without any kind of data.
* We have some hardsubbed karaokes. Hardsubs means the subs are burned into the video and can't be modified or toggled off. We're trying to get rid of those by finding the original videos, so if you have [one of those without subtitles](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=hardsub), you could help us a lot!
* There are some [badly timed karaokes](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=mal+tim%C3%A9), meaning the lyrics don't appear on time to be sung, or appear too soon, etc. If you use Karaoke Mugen and notice one of those, tell us about it!
* We also have [some low quality video files](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=mauvaise+qualit%C3%A9), with a small resolution or bitrate. If you have better quality for any of them, please tell us about it!
* We also have a  [requested list](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=suggestion)! You have a song you'd like to see in karaoke, no problem! Create a suggestion by creating an issue and pray that someone takes care of it. If you want to take care of it yourself, we wrote a [tutorial](https://gitlab.com/karaokemugen/karaokebase/blob/master/docs/french/) (french only for now)!

