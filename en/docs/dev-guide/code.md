# Help with code

Karaoke Mugen is free software to manage your karaoke sessions.

How to :

## Tell us about a bug

* [Create an issue on Shelter's Lab](https://gitlab.com/karaokemugen/karaokemugen-app/issues/new?issue).
* Join your logfile (`karaokemugen.log`) and/or screenshots.
* Describe your bug with as much detail as possible! It helps so we know how to reproduce it and fix it. Help us helping you!

## Code for Karaoke Mugen

* **Karaoke Mugen** is made in NodeJS/React and requires Node version 16. It uses the Electron runtime
* May you be a beginner or an expert, or may you have never touched NodeJS before, you're welcome: few of us knew about NodeJS before beginning to code this project.
* If you want to start browsing the code, don't hesitate to come by our [Discord](http://karaokes.moe/discord) to ask questions on the #dev channel. We'll be listening. You can also [visit our forum](https://discourse.karaokes.moe)
* It takes some time to check out someone else's code. Don't expect to understand everything at once.
* We have [a few on-going issues](https://gitlab.com/karaokemugen/karaokemugen-app/issues/). If you'd like to help, don't hesitate!
