# File formats

## .kara files

[See current documentation on kara files](/user-guide/manage)

### Version 4

Compatible with Karaoke Mugen 3.0.0 and above.

Self-explanatory JSON example :

```JSON
{
  "header": {
    "version": 4,
    "description": "Karaoke Mugen Karaoke Data File"
  },
  "medias": [
    {
      "version": "Default",
      "filename": "ENG - Portal 2 - PC GAME ED - Want You Gone.mp4",
      "audiogain": 1.93,
      "filesize": 30578533,
      "duration": 145,
      "default": true,
      "lyrics": [
        {
          "filename": "ENG - Portal 2 - PC GAME ED - Want You Gone.ass",
          "default": true,
          "version": "Default",
          "subchecksum": "cc116b584fb9097b8246369c45e4f733"
        }
      ]
    }
  ],
  "data": {
    "created_at": "Wed Oct 11 2017 19:01:47 GMT+0200 (GMT+02:00)",
    "kid": "c237d00a-3dd2-4884-9a68-8c0638734dd4",
    "modified_at": "Sun Jul 29 2018 15:12:56 GMT+0200 (GMT+02:00)",
    "repository": "kara.moe",
    "sids": [
      "a7e45824-e4b8-415f-a0ee-125ba7f43a9c"
    ],
    "title": "Want You Gone",
    "platforms": [
      "3c15f758-e1bc-4266-8bb9-3c14112f504a"
    ],
    "tags": {
      "authors": [
        "5bdaba57-27f8-4568-8806-76a807728766"
      ],
      "creators": [
        "4b4e7060-c4a2-42db-bc76-871d266b41fc"
      ],
      "families": [
        "dbedd6b3-d125-4cd8-aa32-c4175e4ca3a3"
      ],
      "groups": [
        "ae613721-1fbe-480d-ba6b-d6d0702b184d",
        "9c79e1bb-a5b9-4059-ad20-4687ec733371"
      ],
      "langs": [
        "de5eda1c-5fb3-46a6-9606-d4554fc5a1d6"
      ],
      "platforms": [
        "3c15f758-e1bc-4266-8bb9-3c14112f504a"
      ],
      "singers": [
        "48c64b08-3394-41c1-a804-a2f26824330e"
      ],
      "songtypes": [
        "38c77c56-2b95-4040-b676-0994a8cb0597"
      ],
      "songwriters": [
        "7658bb92-03da-4d66-905f-8d6b866e4459"
      ]
    },
    "year": 2011
  }
}
```

## .kmplaylist files

#### Sample

```
{
    "Header": {
        "version": 3,
        "description": "Karaoke Mugen Playlist File"
    },
    "PlaylistInformation": {
        "name": "Test import",
        "time_left": 0,
        "created_at": 1519060424.941,
        "modified_at": 1519749130.034,
        "flag_visible": 1
    },
    "PlaylistContents": [
        {
            "kid": "f59fc5f1-bc7a-42a8-b610-337275fbfd63",
            "pseudo_add": "Dummy Plug System",
            "created_at": 1519736989.544,
            "pos": 1,
            "username": "admin"
        },
        {
            "kid": "57595ba8-4eed-4078-990e-4f0fa3d4439a",
            "pseudo_add": "Dummy Plug System",
            "created_at": 1519737017.409,
            "pos": 2,
            "username": "admin"
        },
        {
            "kid": "3f0b8318-eef2-4dbd-9ff6-9b59fffccf70",
            "pseudo_add": "Dummy Plug System",
            "created_at": 1519737240.959,
            "pos": 3,
            "username": "axel"
        },
        {
            "kid": "c747609b-0fd0-468a-9317-eb8de419f615",
            "pseudo_add": "Dummy Plug System",
            "created_at": 1519738409.783,
            "pos": 4,
            "username": "admin",
         	"flag_playing": 1
        },
        {
            "kid": "be4fa49e-35b8-4830-a3c6-0811cbc8e91a",
            "pseudo_add": "Dummy Plug System",
            "created_at": 1519749130.022,
            "pos": 5,
            "username": "admin"
        }
    ]
}
```

## .tag.json files

### Sample

```JSON
{
  "header": {
    "description": "Karaoke Mugen Tag File",
    "version": 1
  },
  "tag": {
    "i18n": {
      "eng": "Porter Robinson"
    },
    "name": "Porter Robinson",
    "tid": "ff2044a6-c11c-4b3a-bab0-1cf3cbc8c745",
    "types": [
      "songwriters"
    ],
    "repository": "kara.moe",
    "modified_at": "2020-03-31T19:04:36.969Z"
  }
}

```

## .kmfavorites files

Favorites list you can import/export

```JSON
{
 "Header": {
  "version": 1,
  "description": "Karaoke Mugen Favorites List File"
 },
 "Favorites": [
  {
   "kid": "...",
   "title": "Mon karaoke favori",
   "songorder": 1,
   "serie": "La série",
   "songtypes": "OP",
   "languages": "jpn"
  }
 ]
}
```

## .karabundle files

This is a bundle of files (lyrics, tags and kara file) you can distribute karaokes with. Once imported, KM App will download the corresponding video file

```JSON
{
    "header": {
        "description": "Karaoke Mugen Karaoke Bundle File"
    },
    "kara": {
        "data": {
            "data": {
                "created_at": "2020-06-23T20:26:00.926Z",
                "kid": "1709c9f9-04d5-4d86-a008-08ef0458e157",
                "modified_at": "2020-06-24T22:44:04.172Z",
                "repository": "kara.moe",
                "sids": [],
                "tags": {
                    "authors": [
                        "87096920-112f-485f-a310-04156cb4624d",
                        "13ddf327-ecde-4c6c-9452-a43fb71db7ab"
                    ],
                    "groups": [
                        "ae613721-1fbe-480d-ba6b-d6d0702b184d"
                    ],
                    "langs": [
                        "4dcf9614-7914-42aa-99f4-dbce2e059133"
                    ],
                    "misc": [
                        "ec4fee75-71fb-45b4-9772-2ca374b4b600",
                        "805c8340-d6a3-4e91-af9a-e7866f2a6654",
                        "86705d4b-24e4-4756-9687-2b9c98bcf366"
                    ],
                    "singers": [
                        "4d675734-308e-460b-9b8b-ee090fddef83",
                        "598804bf-5c62-4ae9-a6aa-cc414eac6781"
                    ],
                    "songtypes": [
                        "97769615-a2e5-4f36-8c23-b2ce2ce3c460"
                    ],
                    "songwriters": [
                        "4d675734-308e-460b-9b8b-ee090fddef83"
                    ]
                },
                "title": "BASSLINE Yatteru?",
                "year": 2018
            },
            "header": {
                "description": "Karaoke Mugen Karaoke Data File",
                "version": 4
            },
            "medias": [
                {
                    "audiogain": -11.13,
                    "default": true,
                    "duration": 286,
                    "filename": "JPN - Camellia, Nanahira - AUDIO OT - BASSLINE Yatteru question_mark.mp3",
                    "filesize": 4902826,
                    "lyrics": [
                        {
                            "default": true,
                            "filename": "JPN - Camellia, Nanahira - AUDIO OT - BASSLINE Yatteru question_mark.ass",
                            "subchecksum": "f16a453ff4762f1cd3f8f970a554f222",
                            "version": "Default"
                        }
                    ],
                    "version": "Default"
                }
            ]
        },
        "file": "JPN - Camellia, Nanahira - AUDIO OT - BASSLINE Yatteru question_mark.kara.json"
    },
    "lyrics": {
        "data": "...", // Stringified ASS data here
        "file": "JPN - Camellia, Nanahira - AUDIO OT - BASSLINE Yatteru question_mark.ass"
    },
    "series": [],
    "tags": [
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "2010s"
                    },
                    "modified_at": "2020-03-31T19:04:33.229Z",
                    "name": "2010s",
                    "repository": "kara.moe",
                    "tid": "ae613721-1fbe-480d-ba6b-d6d0702b184d",
                    "types": [
                        "groups"
                    ]
                }
            },
            "file": "2010s.ae613721.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Audio Only",
                        "fre": "Audio seulement"
                    },
                    "modified_at": "2020-03-31T19:04:33.598Z",
                    "name": "Audio Only",
                    "repository": "kara.moe",
                    "short": "MP3",
                    "tid": "805c8340-d6a3-4e91-af9a-e7866f2a6654",
                    "types": [
                        "misc"
                    ]
                }
            },
            "file": "Audio Only.805c8340.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Camellia"
                    },
                    "modified_at": "2020-06-23T20:30:04.881Z",
                    "name": "Camellia",
                    "repository": "kara.moe",
                    "tid": "4d675734-308e-460b-9b8b-ee090fddef83",
                    "types": [
                        "songwriters",
                        "singers"
                    ]
                }
            },
            "file": "Camellia.4d675734.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Dysp"
                    },
                    "modified_at": "2020-03-31T19:04:34.214Z",
                    "name": "Dysp",
                    "repository": "kara.moe",
                    "tid": "87096920-112f-485f-a310-04156cb4624d",
                    "types": [
                        "authors"
                    ]
                }
            },
            "file": "Dysp.87096920.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Hard Mode",
                        "fre": "Difficile"
                    },
                    "modified_at": "2020-03-31T19:04:34.627Z",
                    "name": "Hard Mode",
                    "repository": "kara.moe",
                    "short": "HRD",
                    "tid": "ec4fee75-71fb-45b4-9772-2ca374b4b600",
                    "types": [
                        "misc"
                    ]
                }
            },
            "file": "Hard Mode.ec4fee75.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "afr": "Japanees",
                        "alb": "Japoneze",
                        "amh": "ጃፓንኛ",
                        "ara": "اليابانية",
                        "arm": "Ճապոներեն",
                        "aze": "Yapon",
                        "bak": "Япон теле",
                        "baq": "Japoniako",
                        "bel": "Японскі",
                        "ben": "জাপানি",
                        "bos": "Japanci",
                        "bul": "Японски",
                        "cat": "Japonès",
                        "chi": "日本",
                        "cze": "Japonské",
                        "dan": "Japansk",
                        "dut": "Japans",
                        "eng": "Japanese",
                        "epo": "Japana",
                        "est": "Jaapani",
                        "fin": "Japanilainen",
                        "fre": "Japonais",
                        "geo": "იაპონელი",
                        "ger": "Japanisch",
                        "gla": "Italian",
                        "gle": "Seapáinis",
                        "glg": "Xaponés",
                        "gre": "Ιαπωνικά",
                        "guj": "જાપાનીઝ",
                        "hat": "Japonè",
                        "heb": "יפנית",
                        "hin": "जापानी",
                        "hrv": "Japanski",
                        "hun": "Japán",
                        "ice": "Japanska",
                        "ind": "Jepang",
                        "ita": "Giapponese",
                        "jav": "Jepang",
                        "jpn": "日本語",
                        "kan": "ಜಪಾನೀಸ್",
                        "kaz": "Жапон",
                        "kir": "Японский",
                        "kor": "일본어",
                        "lat": "Facilisis",
                        "lav": "Japāņu",
                        "lit": "Japonų",
                        "mac": "Јапонски",
                        "mal": "ജാപ്പനീസ്",
                        "mao": "Hapanihi",
                        "mar": "जपानी",
                        "may": "Jepun",
                        "mlg": "Japoney",
                        "mlt": "Ġappuniż",
                        "mon": "Японы",
                        "nep": "जापानी",
                        "nor": "Japansk",
                        "pan": "ਜਪਾਨੀ",
                        "per": "ژاپنی",
                        "pol": "Japoński",
                        "por": "Japonês",
                        "rum": "Japoneze",
                        "rus": "Японский",
                        "sin": "ජපන්",
                        "slo": "Japonský",
                        "slv": "Japonski",
                        "spa": "Japonés",
                        "srp": "Јапански",
                        "sun": "Jepang",
                        "swa": "Kijapani",
                        "swe": "Japanska",
                        "tam": "ஜப்பனீஸ்",
                        "tat": "Япон теле",
                        "tel": "జపనీస్",
                        "tgk": "Ҷопон",
                        "tgl": "Hapon",
                        "tha": "ภาษาญี่ปุ่น",
                        "tur": "Japon",
                        "ukr": "Японський",
                        "urd": "جاپانی",
                        "uzb": "Yaponiya",
                        "vie": "Nhật bản",
                        "wel": "Siapan",
                        "xho": "Isijapanese",
                        "yid": "יאַפּאַניש"
                    },
                    "modified_at": "2020-03-31T19:04:35.131Z",
                    "name": "jpn",
                    "repository": "kara.moe",
                    "tid": "4dcf9614-7914-42aa-99f4-dbce2e059133",
                    "types": [
                        "langs"
                    ]
                }
            },
            "file": "jpn.4dcf9614.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "leonekmi"
                    },
                    "modified_at": "2020-03-31T19:04:35.803Z",
                    "name": "leonekmi",
                    "repository": "kara.moe",
                    "tid": "13ddf327-ecde-4c6c-9452-a43fb71db7ab",
                    "types": [
                        "authors"
                    ]
                }
            },
            "file": "leonekmi.13ddf327.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Long"
                    },
                    "modified_at": "2020-03-31T19:04:35.863Z",
                    "name": "Long",
                    "repository": "kara.moe",
                    "short": "LON",
                    "tid": "86705d4b-24e4-4756-9687-2b9c98bcf366",
                    "types": [
                        "misc"
                    ]
                }
            },
            "file": "Long.86705d4b.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Nanahira"
                    },
                    "modified_at": "2020-06-23T20:30:04.982Z",
                    "name": "Nanahira",
                    "repository": "kara.moe",
                    "tid": "598804bf-5c62-4ae9-a6aa-cc414eac6781",
                    "types": [
                        "singers"
                    ]
                }
            },
            "file": "Nanahira.598804bf.tag.json"
        },
        {
            "data": {
                "header": {
                    "description": "Karaoke Mugen Tag File",
                    "version": 1
                },
                "tag": {
                    "i18n": {
                        "eng": "Other",
                        "fre": "Divers"
                    },
                    "modified_at": "2020-03-31T19:04:36.843Z",
                    "name": "OT",
                    "repository": "kara.moe",
                    "tid": "97769615-a2e5-4f36-8c23-b2ce2ce3c460",
                    "types": [
                        "songtypes"
                    ]
                }
            },
            "file": "OT.97769615.tag.json"
        }
    ]
}
```