# API Documentation

API documentation is automatically generated with every build, please check which version you're using for your dev.

[Read the API doc](https://api.karaokes.moe/app/)