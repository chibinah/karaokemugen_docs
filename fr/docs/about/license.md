# Licence

## Logiciel

Les logiciels Karaoke Mugen (l'application, le serveur, l'exploreur, etc.), sont, sauf mention contraire, sous licence MIT

## Base de karaokés

Les bases de données de Karaoke Mugen (Base OTAKU et WORLD) sont disponibles sous différentes licences selon le contenu dont il est question. La licence complète est disponible [ici](https://gitlab.com/karaokemugen/bases/karaokebase/-/blob/master/LICENSE.md)
## Documentation

Cette présente documenation est disponible sous les termes de la [licence Creative Commons 4.0 BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/4.0/).

