# Feuille de route

L'application possède des "milestones", des points d'étape.

[Consulter les points d'étape de **Karaoke Mugen**](https://gitlab.com/karaokemugen/karaokemugen-app/-/milestones)

Ces points d'étape permettent de voir quelles fonctionnalités sont attendues dans telle ou telle version. Parfois, une fonctionnalité ou un bug peut être déplacé à un point d'étape ultérieur selon le temps qu'il prend, de quoi il dépend ou si cela représente un gros travail.
