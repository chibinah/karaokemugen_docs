# Contact

Que vous ayez une suggestion, une demande, ou besoin d'aide, plusieurs moyens pour nous contacter sont à votre disposition :

- Par mail : `karaokemugen at shelter.moe`
- Par Twitter : [@KaraokeMugen](http://twitter.com/karaokemugen)
- Par Discord : [Canal #karaoke-fr](http://karaokes.moe/discord)

## Bugs, améliorations...

Si vous rencontrez un bug, merci de le signaler :

- S'il s'agit d'un bug de l'application ou une suggestion d'amélioration, merci de [créer une issue sur le projet de l'application du GitLab de Shelter](https://gitlab.com/karaokemugen/karaokemugen-app/-/issues/new?issuable_template=Bug)
- S'il s'agit d'un karaoké mal ajusté dans notre base, vous pouvez signaler un karaoké directement depuis [l'explorateur de karaokés](https://kara.moe/base/), allez sur la page du karaoké qui pose problème et dans la section "Un problème ?", vous pouvez soit Signaler un problème, ce qui crééra un ticket dans [le système de suivi des problèmes de la base](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues). Si vous pensez pouvoir régler le problème par vous-même (merci !), vous pouvez cliquer sur Proposer une modification, modifier ce qui va pas et soumettre votre proposition, elle sera là aussi enregistrée sous la forme d'un ticket dans [le système de suivi](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues). Plus d'informations dans ["Participer à la base"](../contrib-guide/base.md#Améliorer-les-données).