## Commencer à développer sur Karaoke Mugen

Pour bien commencer, il vous faut de bons outils. Ce guide est prévu pour Windows mais ne diffère pas beaucoup si vous êtes sous Linux ou macOS

On donne un moyen d'installer les applications nécessaires mais n'oubliez pas que vous pouvez aussi choisir les outils avec lesquels vous avez le plus l'habitude de travailler, par exemple en remplaçant Fork par Gitkraken ou git Bash par le WSL.

### Installer git / fork

- Récupérez [git](https://git-scm.com/)
- [gitAhead](https://gitahead.com/) est un bon client graphique multiplateforme
- [Fork](https://git-fork.com/) est un client graphique très pratique pour git, open source et multiplateforme, mais payant.

En général utiliser git bash (fourni avec git) est un bon terminal.

### Installer Visual Studio Code

- [Installez Visual Studio Code](https://code.visualstudio.com/)

Il y a de bonnes extensions à installer :

- Beautify
- better Comments
- **ES Lint** (indispensable car il va sous-ligner les petits défauts de code pour vous comme l'indentations, les ; de fin de commande, etc. qui vont vous faciliter la vie grandement !
- gitignore
- GitLens (pour éventuellement faire vos commits depuis VSCode ou voir facilement qui a écrit cette ligne de code)
- gitmoji
- markdownlint
- Node.js modules intellisense (auto-complétion des noms de modules JS)
- npm intellisense (auto-complétion NPM)
- SQL Beautify
- vscode-icons
- YAML

### Installer et préparer PostgreSQL

[L'installeur Windows de EnterpriseDB](https://www.postgresql.org/download/windows/) fera parfaitement l'affaire car il installe PostgreSQL en tant que service sous Windows et il est livré avec pgAdmin pour s'occuper de la base au besoin.

Une fois installé, vous devrez créer un user superadmin. Lancez la commande dans le dossier `bin` de votre dossier PostgreSQL (par défaut `C:\Program Files\PostgreSQL\13\bin`).

```
createuser -h localhost -P -s postgres
```

Si ça vous dit que le rôle existe déjà, tant mieux. Sinon entrez le mot de passe `musubi`.

Connectez-vous avec ce user postgres via :

```
psql -U postgres
```

Soit il vous demandera votre mot de passe définit plus haut soit rien du tout si le user existait déjà.

On va créer le user et base de données de KM App :

```SQL
CREATE DATABASE karaokemugen_app ENCODING 'UTF8';
CREATE USER karaokemugen_app WITH ENCRYPTED PASSWORD 'musubi';
GRANT ALL PRIVILEGES ON DATABASE karaokemugen_app TO karaokemugen_app;
```

On doit ajouter une extension à la base, donc :

```SQL
\c karaokemugen_app
```

Le prompt change pour indiquer qu'on est sur la base nouvellement crée. Puis :

```SQL
CREATE EXTENSION unaccent;
```

Puis `\q` pour quitter.

### Installer NodeJS

[Installez NodeJS depuis son site web.](https://nodejs.org/fr/) Normalement il faut prendre la dernière version LTS, sauf cas contraire mais c'est uniquement quandon fait des tests de montée de version de Node

### Installer Yarn

[Yarn est un lanceur et gestionnaire de packages comme npm. Installez-le.](http://yarnpkg.com)

### Cloner le dépôt Karaoke Mugen

Faites-vous un dossier par exemple `C:\dev` sous Windows.

Ouvrez votre terminal favori et allez dans le dossier en question pour taper :

```
git clone https://gitlab.com/karaokemugen/karaokemugen-app.git
```

Vous pouvez aussi cloner en SSH si vous préférez. Assurez-vous d'avoir une clé SSH et de l'indiquer dans votre profil sur Gitlab.

Puis allez dans le dossier crée par le clone `karaokemugen-app`.

#### Configuration de git

Pour vous faciliter la tâche on a préconfiguré git pour vous. Pour activer ça, lancez

```
yarn gitconfig
```

Ça va activer diverses options :

- Afficher le diff des sous-modules
- Faire en sorte que les pull/fetch/status et push aillent récursivement dans les sous-modules.

Pour push vos commits vous aurez besoin du statut de Développeur sur Gitlab, demandez-le à AxelTerizaki sur Discord. Vous pouvez également faire un fork chez vous et proposer des merge request si ça vous va mieux

Vous aurez également besoin, si vous n'utilisez pas SSH, d'un token au moment de push. Vous pouvez générer ce token depuis votre page paramètres de gitlab dans la section "Access Token" / "Jetons d'accès"

### Installation et configuration Node

Tout est automatisé, il vous suffit de taper

```
yarn setup
```

`setup` est en fait une commande qui va faire plusieurs choses :

- L'installation des dépendances node pour les 3 parties sus-citées (`yarn install` dans les 3 dossiers)
- La transpilation du code Typescript/React en code JS utilisable via `yarn build` dans chaque dossier.

Selon ce que vous modifiez, utilisez bien les bonnes commandes dans le bon dossier. `yarn setup` peut être long et pas toujours nécessaire.

### Installer les dépendances binaires

Vous aurez besoin de [ffmpeg](http://ffmpeg.org) et [mpv](http://mpv.io). Téléchargez-les et dézippez-les dans le dossier `app/bin` (creez-le si nécessaire)

### Récupérer une base de karaokés complète

Allez dans `app` et clonez Karaokebase :

```
git clone https://gitlab.com/karaokemugen/karaokebase.git
```

#### Les médias

Les médias prennent plusieurs centaines de giga-octets **mais ne sont pas nécessaires pour faire du dev**. Sauf si vous bossez sur le player, dans ce cas il est recommandé de se rabattre sur les samples fournis.

Ils peuvent être mis à jour ou en lançant KM avec `--updateMedias`.

### Configurer KM

Créez un fichier `config.yml` dans `app` avec le contenu suivant :

```YAML
Online:
  Stats: false
  ErrorTracking: false
System:
  Database:
    bundledPostgresBinary: false
    database: karaokemugen_app
    host: localhost
    password: musubi
    port: 5432
    superuser: postgres
    superuserPassword: musubi
    username: karaokemugen_app
  Repositories:
    - Name: kara.moe
      Online: true
      Enabled: true
      Path:
        Karas:
          - karaokebase/karaokes
        Lyrics:
          - karaokebase/lyrics
        Medias:
          - karaokebase/medias
        Tags:
          - karaokebase/tags
```

Avec cette configuration KM ira chercher tous les karas de votre karaokebase que vous avez cloné tout à l'heure depuis le git.

Aussi, les stats ne seront pas envoyées afin de ne pas embrouiller KM Server avec des stats faussées par vos essais.

### Lancement

*musique de Gundam*

Une fois que tout est prêt, on peut lancer via

```
yarn start
```

`start` va transpiler KM du Typescript au Javascript à chaque lancement. Cela peut prendre de nombreuses secondes mais votre programme lancé sera toujours à jour.

### Annexes

#### Structure du projet KM

KM est découpé en deux grandes parties :

- Un frontend React :
  - `kmfrontend` est l'interface mobile et PC dédiée à la gestion d'une session karaoké, que ça soit pour les opérateurs de karaoké ou leur public.
- Un backend NodeJS dans `src`.

Pour ces deux parties, nous utilisons Typescript pour gérer le typage de l'application.

Il y a également un dossier `migrations` contenant les migrations SQL servant à gérer la structure de la base et ses mises à jour.

##### La librairie Karaoke Mugen

Accessoirement KM utilise une librairie de fonctions partagée avec KM Server et sobrement appelée KM Lib. Cette lib se trouve dans `src/lib` et est un sous-module git. Pour comprendre comment ça marche, [cette petite doc est très bien](https://delicious-insights.com/fr/articles/git-submodules/). Un sous-module git nous permet notamment de versionner la librairie, d'en faire des branches, bref d'avoir du code commun réutilisable.

La lib est mise à jour automatiquement par un `pull` de l'app KM, mais si jamais vous y touchez, il faudra penser à `commit` et `push` dans le dossier `src/lib` avant d'ajouter ce dernier à KM App et de `commit` et `push` sur KM App. La doc sus-citée vous donnera plus d'informations pour comprendre les sous-modules git.

La lib contient notamment des fonctions pour :

- L'accès à la base de données postgresql ainsi que pour gérer le filtre de la liste des karaokés
- La définition des formats de fichiers kara, tags...
- La génération de la base de données à partir des fichiers de karaokebase
- L'interfaçage avec un gitlab
- La création de fichier karaoké
- Des types communs à KM Server et KM App
- Des utilitaires divers comme :
	* une barre de progression
	* Manipulation de fichiers ASS
	* Manipulation de dates
	* ffmpeg (pour lire les infos d'une vidéo ou calculer le gain audio)
	* Accès aux fichiers (lecture, écriture, etc)
	* Journalisation (logger)
	* Génération des prévisualisations de vidéos
	* Validateurs des données entrantes (contrôles)
	* Websockets et système PubSub interne

##### Structure du backend

Ceci est un tour d'horizon basique du backend :

- Tout démarre dans `index.ts`, il suffit de suivre la fonction `main()`.
- Le dossier `components` contient les principaux modules de KM, par exemple son moteur, l'init du frontend et mpv, son player vidéo
- Le dossier `electron` contient tout le code relatif à l'application Electron et son interface graphique
- Le dossier `services` contient tout le code applicatif et la logique. Manipulation des playlists, des karas, du player, et de tout autre sous-système de KM. Les `services` fournissent des fonctions aux `controllers` et piochent leurs données via les fonctions de `dao`.
- Le dossier `controllers` contient toutes les routes d'API appellées par le frontend de KM. Les routes d'API appellent les fonctions des `services`.
- Le dossier `dao` contient toutes les fonctions faisant entrer et sortir des données de Karaoke Mugen : écrire et lire dans la base de données, dans les fichiers, etc.
- Le dossier `types` contient toutes les définitions de types utilisés dans l'application.
- Le dossier `utils` contient une myriade d'utilitaires appelés à différents endroits : gestion de la configuration, de l'état, les constantes, les paramètres par défaut, le téléchargement, la manipulation de git, twitch, ou postgresql, etc.

#### Versions

La numérotation des versions suit le semver classique : majeure.mineure.sous-version

- 2.1.0 est une version majeure de la base de code 2.x.
- 2.2.1 est une correction de la 2.2.
- 3.0.0 est une nouvelle version majeure.

De plus chaque version a un nom de code. Chaque version majeure est un personnage féminin d'animation japonaise, et chaque version mineure un adjectif portant la même première lettre que l'héroine. Exemples :

- Akari Amoureuse
- Belldandy Bolchévique
- Chitoge Chatoyante
- Finé Fiévreuse
- Finé Fantastique
- etc.

On se débrouille pour trouver un fond d'écran pour chaque version.

#### Structure du git

Il y a deux branches :

- `master` est la branche stable. Si on a un fix à faire sur une version actuellement stable on fera le fix sur cette branche. On merge `next` dans `master` lors des versions majeures.
- `next` est la branche de développement. C'est ici qu'on fera toute modification ou qu'on fera un merge d'une branche de fonctionnalité.

Lorsque l'on travaille sur une issue, on lui crée une branche dédiée. Gitlab s'en occupe pour nous en créant une merge request et une branche automatiquement en appuyant sur le bouton adéquat sur la page de l'issue. **Pensez à indiquer `next` comme branche source quand vous le faites.**

#### Créer une nouvelle migration

On utilise l'outil postgrator. S'il n'est pas installé correctement par yarn :

```sh
npm install -g postgrator
```

Puis lancement via pour effectuer les migrations sans Karaoke Mugen :

```sh
yarn postgrator
```

Pour créer une migration il faut créer notamment deux fichiers SQL dans `migrations` qui s'appellent chacun `do` et `undo`. Regardez d'autres exemples fournis.

Pensez à écrire votre migration en étape par étape puis à écrire les migrations de retour arrière (down) dans un brouillon avant de les mettre dans les fichiers .sql et de les tester sur KM.