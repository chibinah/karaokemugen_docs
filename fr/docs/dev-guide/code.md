# Participer au code

**Karaoke Mugen** est un logiciel libre de gestion de sessions de karaoké.

Comment faire pour :

## Signaler des bugs

* [Créez une issue sur le Lab de Shelter](https://gitlab.com/karaokemugen/karaokemugen-app/issues/new?issue).
* Pensez à vous munir de votre fichier de log (`karaokemugen.log`) ou de captures d'écrans éventuelles.
* Décrivez votre bug avec détail ! Cela aide réellement à savoir comment le reproduire et essayer de trouver une solution. Aidez-nous à vous aider !

## Développer pour Karaoke Mugen

* **Karaoke Mugen** est développé en NodeJS et demande la version 16 minimum.
  * Le code est en Typescript
* Que vous soyez débutant, confirmé ou que vous n'ayez jamais touché NodeJS, vous êtes le bienvenu : peu d'entre nous connaissaient NodeJS avant de commencer le développement de l'application. 
* Si vous voulez commencer à fouiller dans le code, n'hésitez pas à passer sur [Discord](http://karaokes.moe/discord) pour poser des questions sur le canal #dev. Les développeurs sont à votre écoute. Vous pouvez aussi poser des questions [sur notre forum](https://discourse.karaokes.moe)
* Cela prend du temps de se plonger dans le code de quelqu'un d'autre. Ne vous attendez pas à comprendre un projet de cette taille en un claquement de doigts.
* Il y a un [petit paquet d'issues](https://gitlab.com/karaokemugen/karaokemugen-app/issues/) dont chacun peut s'occuper s'il s'en sent capable.
