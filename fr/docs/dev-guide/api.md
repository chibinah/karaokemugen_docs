# API

La documentation de l'API a été écrite en anglais initialement.

Elle est générée automatiquement à chaque build, veillez à utiliser la documentation de la version que vous visez pour votre développement.

[Lire la documentation de l'API](http://api.karaokes.moe/app)