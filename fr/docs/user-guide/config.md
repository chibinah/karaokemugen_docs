# Configurer votre karaoké

Une fois que Karaoke Mugen fonctionne bien, vous voudrez peut-être explorer les possibilités offertes et les différents paramètres que vous pouvez changer pour créer l'expérience de karaoké à votre goût.

Accéder aux paramètres se fait via le menu K en haut à droite de l'[interface opérateur](getting-started.md#linterface-operateur), bouton "Options". Alternativement, dans la barre de menu de l'application, dépliez l'item "Options" puis "Préférences". Nous nous intéresserons à la section Karaoké, qui est la section la plus complète donc de fait compliquée à appréhender.

## Limites et quotas

Dans sa configuration par défaut, Karaoke Mugen permet à chacun d'ajouter un nombre illimité de chansons à la liste de lecture. Il n'y a donc aucune limite sur le nombre de chansons que les utilisateurs peuvent ajouter. Vous avez à disposition plusieurs outils pour instaurer des quotas aux utilisateurs. *Ces quotas ne s'appliquent pas aux opérateurs.*

Il existe deux types de quota :

- Le **quota par nombres de chansons** permet de restreindre les ajouts par les utilisateurs selon le nombre de chansons qu'ils ajoutent.
- Le **quota par durée** permet quant à lui de restreindre la durée cumulée d'ajouts des utilisateurs. Si vous le configurez à 20 minutes, chaque utilisateur pourra ajouter autant de musiques qu'il veut du moment que la durée cumulée ne dépasse pas 20 minutes.

Les deux modes sont exclusifs, vous ne pouvez pas cumuler les deux. Le compteur de karaokés s'affiche au public dans la barre supérieure sous la forme ![Karaokés restants : ∞](../img/user/quota_demo.png)&nbsp;.

### Libération

L'utilisateur récupère son quota, au choix :

- quand la chanson passe à l'écran
- quand la chanson est libérée par l'opérateur (action possible dans le menu clé à molette&nbsp;:fa-wrench:)
  - la chanson est libérée automatiquement lorsque vous approuvez ou refusez la chanson
- si la libération par vote est activée, lorsque d'autres personnes soutiennent l'ajout.

### Libération par vote

Vous pouvez configurer votre karaoké pour que si le kara reçoit le plébiscite ("like") d'autres utilisateurs, la personne qui l'a initialement ajoutée reçoit de nouveau son quota utilisé pour ajouter la chanson. Vous pouvez donc configurer un pourcentage minimum d'utilisateurs connectés et un minimum absolu d'utilisateurs. Il suffit qu'un des deux critères soit satisfait pour libérer le karaoké.

## Listes de lecture

### Ajout intelligent et équilibrage

Si vous êtes entre amis et que le [karaoké se gère tout seul](playlists.md#liste-a-la-fois-publique-et-courante), il est possible d'activer l'ajout intelligent ou l'équilibrage de playlist. Ces deux mécanismes se chargent d'éviter les monopoles, pour qu'une personne bombarde la playlist avec ses propres chansons ne prenne pas toute la place.

L'ajout intelligent ajoute au-dessus de toutes les autres chansons dès que l'utilisateur n'a plus de chansons à faire passer. L'équilibrage passe, dans la mesure du possible, une chanson par utilisateur distinct (une chanson de l'utilisateur 1, puis de l'utilisateur 2, puis 3, plus de nouveau 1, 2, etc...).

### Intermissions (Médias de playlist)

Par défaut, pour agrémenter vos karaokés, Karaoke Mugen affichera une sélection de jingles, sponsors durant la lecture. Il y a aussi une introduction et des outros. Des vidéos sont embarqués et vous pouvez [ajouter les vôtres](advanced.md#medias-de-liste-de-lecture). Pour les jingles et les sponsors vous pouvez configurer l'intervalle entre 2 jingles ou sponsors. Pour les autres, vous pouvez configurer un message qui s'affiche automatiquement.

## Session de karaoké

### Karaoké classique

Le mode Karaoké classique sert à reproduire l'expérience qu'on peut trouver en salle de karaoké, à savoir que le lecteur se mettra en pause à chaque chanson et permettra à celui qui a ajouté la chanson de la démarrer (un opérateur peut aussi démarrer la chanson). Parfait quand vous avez une salle avec un micro à donner.

### Mode Stream

Le mode stream permet de marquer une pause (le délai est configurable) entre chaque chanson pour permettre à quelqu'un qui enchâine tous les karaokés de boire, respirer, etc. (utile en somme !).

#### Chat Twitch

De plus, si vous êtes prêt à chanter tout ce que vous suggère votre public, vous pouvez activer le sondage via le chat Twitch, les personnes seront donc en mesure de voter dans le chat parmi les chansons suggérées par les viewers de votre stream. Notez tout de même que vous n'aurez donc plus aucun moyen de contrôler les karaokés en amont, n'oubliez donc pas d'[exclure les karaokés](playlists.md/#listes-noires) que vous ne voulez pas voir (pensez à exclure les karaokés pour adultes, épilepsie, il y a des tags divers pour ça dans la base).

### Notification de fin de session

Si vous configurez un délai en minute et que vous [configurez la session](getting-started.md#sessions) avec une durée de fin, vous recevrez une notification vous invitant à éventuellement préparer la fin de votre évènement si vous avez une scène limitée dans le temps.

### Vote du public

Envie de laisser le public choisir ses chansons ? Le vote du public propose durant les chansons de voter pour la prochaine parmi un choix de 4 chansons. L'interface web proposera à vos utilisateurs de voter pour le karaoké qu'ils souhaitent voir ensuite. Les karaokés sont sélectionnés depuis la [liste publique](playlists.md#liste-de-lecture-publique).

S'il y a une égalité entre deux ou plus de karaokés, le karaoké à passer sera choisi au hasard parmi ceux ayant le meilleur score.

C'est un mode idéal pour ne pas se prendre la tête et d'avoir un karaoké qui s'administre tout seul.

## Ajouts mystère

L'opérateur peut décider si ce que les gens ajoutent est visible ou non dans la playlist. Les chansons invisibles comptent dans la liste mais sont affichés avec un nom configurable (par défaut ???).

Idéal pour faire des surprises à vos invités, ou simplement laisser planer le mystère sur ce qui va passer ensuite.

L'opérateur peut faire en sorte que tous ses ajouts soient considérés comme mystère également.
