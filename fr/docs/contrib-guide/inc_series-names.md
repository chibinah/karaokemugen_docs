- Pour tout ce qui est animation :
	- [anidb](https://anidb.net/) (onglet _songs_ sur la page de l'anime, après la section _Anime Tags_)
	- [MyAnimeList](http://myanimelist.net/)
	- [vgmdb](http://vgmdb.net/)
	- [Planète Jeunesse](http://www.planete-jeunesse.com/)
	- Les wikia spécialisés
	- Wikipédia (Français, Anglais, Japonais...)
	- [NicoNico](https://www.nicovideo.jp/) / [BiliBili](https://www.bilibili.com/) (pour les AMV notamment)  

- Pour les jeux vidéo et les Visual Novels :
	- [vndb](https://vndb.org/)
	- Les wikia spécialisés
	- Wikipédia (Français, Anglais, Japonais...)

- Pour les tokusatsu / séries live :
	- Les wikia spécialisés
	- Wikipédia (Français, Anglais, Japonais...)

- Et de manière générale :
	- Google (!)	