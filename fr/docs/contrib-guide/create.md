# Créer un karaoké

Vous ne trouvez pas votre générique préféré dans la base de Karaoke Mugen et ça vous rend très triste ? On a la solution !

Avant toute chose, vérifiez bien à deux fois que le karaoké est bien indisponible :

- Rendez vous [à cette adresse](https://kara.moe) qui regroupe *tous* les karaokés disponibles. Faites des recherches pour voir si votre générique est bien dans la base (ou pas).
- Rendez-vous aussi sur le GitLab de la base pour voir les karaokés [qui vont être bientôt ajoutés](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=To%20Add) et [ceux en cours](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=Doing). 

## J'ai bien tout vérifié, il n'y a rien

**Si votre générique n'est pas dans la base et qu'aucune issue n'a été créée, deux solutions s'offrent à vous :** soit vous déposez une demande via [le site kara.moe](https://kara.moe) en cherchant le titre de votre karaoké et en cliquant sur le lien tout en bas de la liste pour nous le suggérer. Espérez ensuite qu'une brave âme fasse le travail... Soit vous le réalisez vous-même ! C'est simple, et on va tout vous expliquer dans ce tutoriel.

## Créer son karaoké

**NOTE : Dans les sections suivantes, vous trouverez des conseils et recommandations concernant les tailles et formats de vidéos à respecter, la transcription du japonais et d'autres informations concernant le karaoké. Ce sont des RECOMMENDATIONS. Vous N'AVEZ PAS à les suivre si vous faites des karaokés pour votre propre base de données.**

Il est recommandé de suivre les étapes ci-dessus dans l'ordre.

- [Réunir le matériel](material.md)
- [Réaliser le karaoké en lui-même](karaoke.md)
- [Tester votre karaoké](test.md)
- [Envoyer votre karaoké](upload.md)

## Infomations complémentaires

- [Éditer un karaoké](editkaraoke.md)
- [Créer un fichier de métadonnées ".kara.json"](karafile.md)
- [Outils de dépannage & erreurs récurrentes](troubleshoot.md)
- [Documents de références](references.md)
- [Foire aux questions](faq.md)