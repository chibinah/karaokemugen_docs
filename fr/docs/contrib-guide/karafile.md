# Créer ou modifier des fichiers .kara.json

C'est un fichier de métadonnées pour votre karaoké. Il est primordial : il indique à Karaoke Mugen où sont les fichiers à utiliser ainsi que d'autres infos pour générer sa base de données.

On y trouve aussi toutes les métadonnées concernant votre karaoké : le nom du chanteur, du parolier, du studio ayant réalisé la vidéo, la date de création ou la langue utilisée pour chanter.

Ici, nous allons voir comment créer ou modifier ce fichier via une interface graphique.

## Accès à l'interface

L'interface de gestion des karaokés se trouve dans Karaoke Mugen. Lancez le programme et sur l'écran d'accueil, cliquez sur le bouton SYSTEME (vous pouvez aussi y acceder via l'onglet en haut de la fenêtre `Allez à` > `Panneau Système`).

![ass](../img/creation/System.png)

Une fois dans la partie système, survolez le 3ème onglet **Karaokés** puis cliquez sur **Nouveau** pour accéder au formulaire de création de .

![ass](../img/creation/SystemTab.png)

## Remplir les informations du karaoké

Vous voilà donc dans le formulaire de création de kara.

![ass](../img/creation/Karacreatepage.png)

La série, tout comme les éléments suivants, sont des tags. Les tags ont leur propre [fichier de données](tag.md), qui contient les éventuelles variations régionales et alias.

Soit vous avez une liste de cases à cocher, auquel cas vous pouvez cocher les cases appropriées, soit vous avez un champ avec auto-complétion qui vous proposera soit de cliquer sur un tag existant soit de créer un tag directement.

Si vous êtes amené à créer des tags, vous pourrez, après la création de votre karaoké, éditer les tags pour ajouter des informations supplémentaires.

### Fichier vidéo

C'est votre source vidéo (ou audio si vous avez timé un mp3). Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Fichier de sous-titres

C'est votre *time*, votre *.ass*. Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Karaokés parents

En selectionnant un karaoké parent, non seulement cela copie les informations du parent sur votre karaoké, mais le définit également comme "enfant" du karaoké parent. Cela veut dire que les visiteurs verront que le parent a différentes versions enfant. Par exemple il peut s'agir de toutes les versions différentes de l'opening Sakura Taisen, ou par exemple une *music video* qui a comme parent un *opening*.

En selectionnant le parent, les visiteurs peuvent plus facilement découvrir qu'il existe des versions alternatives.

Un karaoké peut avoir plusieurs parents.

### Titre du morceau

C'est le titre de la chanson de votre karaoké. Il peut exister en plusieurs langues et avoir des alias. Les alias servent au moteur de recherche et vous pouvez indiquer des mots qui ne se trouvent pas déjà dans le(s) titre(s) du karaoké.

En général une chanson n'a qu'un seul titre mais il arrive parfois qu'elle en ait un en anglais, et un différent en japonais par exemple.

<details>
<summary>(Dérouler) <em>Concernant les chansons en japonais</em></summary>
<p>
Référez-vous [aux normes de typographie plus bas](karafile.md#typographie) pour bien écrire votre titre.

Une chanson doit obligatoirement avoir au minimum un titre en langue anglaise. Pour le japonais, vous pouvez mettre le titre "Japonais" (en kana) et le titre en "Japonais - Romaji" en romaji.

___

</p>
</details>

### Versions

Les tags *Versions* vous permettent de préciser une altération d'une chanson, pratique pour les cas où plusieurs karaokés représentent la même chanson, mais que c'est une reprise, une version instrumentale, etc.

Lorsque vous allez créer votre karaoké, les versions vont être disponibles sous forme de cases à cocher (vous pouvez en cocher plusieurs). Si vous devez préciser une variation ne figurant pas dans la liste (comme par exemple une variation liée à un épisode dans la série ou la même chanson mais chantée par un autre personnage), vous pouvez le préciser après le titre de la chanson en adoptant la forme `<titre> ~ <version> Vers.`.  
  
Par exemple [`irony ~ Ep. 12 Vers.`](https://kara.moe/base/kara/irony-ep-12-vers/b9e80280-0d04-415a-bca3-2fd2681043e2), remarquez que le tag alternative est présent.

Ce n'est pas parce que vous précisez une version grâce à cette notation manuelle que vous ne pouvez pas mettre de tag version (ceux des cases à cocher).

<details>
<summary>(Dérouler) <em>A propos du tag *Complète*, *Courte*, des concerts, de la vie et de tout le reste</em></summary>
<p>

On ne met jamais le tag *Complète* sur les concerts. En dehors de ce cas précis, si l'on ajoute une chanson en version *complète*, on coche la case *Complète* uniquement si cette chanson existe déjà dans la base sous une autre forme (dans la majeure partie des cas, en version courte).  

Exemple :  

- `ENG - Kiznaiver - OP - LAY YOUR HANDS ON ME` et  
- `ENG - Kiznaiver - MV - LAY YOUR HANDS ON ME [Version Complète]`

On ne met jamais le tag *Courte* sur un opening / ending / insert song extraite d'un anime etc. Le tag de version *Courte* se place si on possède également la version complète dans la base, pour bien les distinguer.

___

</p>
</details>

### Série(s)

Si votre chanson n'est pas issue d'une série télévisée ou d'un film, vous pouvez passez cette section.

C'est ici que vous ajoutez la série dont est issue votre karaoké (ou plusieurs si vous faites des AMV par exemple).

![ass](../img/creation/KaracreatepageSerie.png)

### Types de chanson

<details>
<summary>(Dérouler) <em>A propos des vidéos amateur</em></summary>
<p>
**Une vidéo non officielle / amateur est une AMV. Une vidéo officielle est une MV.**
</p>
</details>

<details>
<summary>(Dérouler) <em>A propos des karaokés audio uniquement</em></summary>
<p>
Si vous ajoutez un MP3, cochez la case `Audio uniquement`, puis choisissez en complément le type qui convient le mieux à la chanson. Si par exemple c'est une chanson que l'on entend dans la série (même dans une version raccourcie), mettez *Opening*, *Ending* ou *Insert Song* en fonction. Si c'est une version complète, choisissez l'un de ces derniers, et cochez *Complète* dans la section [Versions](#versions). 

___

</p>
</details>

Si la chanson n'est pas présente dans la série, vous avez le choix entre `character song` et `image song`, voir carrément `autre`. S'il s'agit d'une chanson provenant d'un single ou d'un album, cochez simplement `Audio uniquement`.

<details>
<summary>(Dérouler) <em>A propos des character song et image song</em></summary>
<p>
- Une Image Song est une chanson utilisée à but promotionnel et **non utilisée** dans l'anime (par exemple l'album "Negai ga Kanau Basho ~Vocal&Harmony version~" de CLANNAD est un image album et contient donc des image songs).

- Une Character Song est une chanson utilisée à but promotionnel, non utilisé dans l'anime et qui a pour vocation d'approfondir le personnage qu'elle va traiter. Elle n'est **pas forcément chantée** par le seiyû du personnage traité. On trouve des Character Song soit dans des albums qui leur sont exclusivement dédiés, soit ajoutées dans les singles ou l'OST de l'anime.<br/>

- À ne pas confondre avec une Character Version, qui est une reprise d'une chanson, chantée par un personnage différent de la performance originale (exemple, le Hare Hare Yukai chanté par Kyon). Dans ce cas-là ne pas oublier d'ajouter `~ [nom du personnage] Vers.`.

___
</p>
</details>

N'utilisez `autre` qu'en dernier recours.

### Numéro de chanson

Les numéros de chanson n'affectent en général que les chansosn issues d'animés japonais.

<details>
<summary>(Dérouler) <em>A propos des numéros de chanson</em></summary>
<p>

Pour numéroter **uniquement les openings et les endings**, nous ne numérotons pas le reste.

Si la série ne contient qu'un seul opening / ending, ce champ doit être **vide** (et pas "0").

Sur anidb, si le premier épisode utilise comme ending la musique qui deviendra l'opening dans les épisodes suivants, n'en tenez PAS compte, ce n'est PAS un ending, cela reste avant tout l'opening de la série.

___

</p></details>

### Langues(s)

La langue dans laquelle votre karaoké est interprêté. Comme les séries, une liste existe déjà.

- Si c'est une langue inconnue / inventée, ajoutez *"Langue inconnue"*.
- Si votre karaoké ne contient aucun chant de base (et donc que votre .ass est vide), ajoutez *"Pas de contenu linguistique"*.
- **Vous pouvez ajouter jusqu'à 2 langues sur le même karaoké. Passé ce chiffre, vous ne devez mettre que *"Multi-langues"*.**
- Si votre karaoké compte autant ou *quasi* autant de lignes "langue 1" que de lignes "langue 2", indiquez les 2 langues. À l'inverse, si une langue domine sur l'autre, n'indiquez que celle-ci.
- Si vous avez timé une version *instrumentale* (donc sans parole), indiquez tout de même la langue dans laquelle le kara sera chanté par le public.

### Année de diffusion

L'année **où votre vidéo a été diffusé** (et non l'année de commencement de votre anime, pas de "1999" sur l'OP 18 de One Piece).

### Chanteur(s)

Le(s) interprète(s) de votre karaoké.

### Compositeurs(s)

Le(s) auteur(s), compositeur(s), arrangeur(s) **et** parolier(s) de votre karaoké. Comme les séries, une liste existe déjà, vérifiez que votre/vos compositeur(s) n'est pas déjà dedans pour ne pas créer un doublon.

### Créateur(s)

L'entité à l'origine de votre vidéo (des studios d'animation ou de développement de jeux ou des labels de musique dans le cas de chansons simples). 

<details>
<summary>(Dérouler) <em>Selon votre type de chanson...</em></summary>
<p>

- Si vous vous occupez une AMV, mettez le studio **et** le créateur de l'AMV.
- Pour les jeux vidéo, mettez l'entité qui a **développé** le jeu, pas celle qui le vend / publie.
- Si c'est une MV live avec des vrais gens, mettez le label du groupe.

___

</p></details>

### Auteur(s) du karaoké

C'est vous ! Tapez votre nom ou pseudo pour vous ajouter dans la liste des "Karaoke Maker".

### Autres tags

- `Famille(s)` : Type de famille du karaoké.
	- **À titre d'information et pour éviter les confusions, le tag "Anime" concerne TOUT ce qui est animé, que ça soit un anime japonais, un cartoon américain, une production française, etc. Si votre vidéo n'est pas fait avec une technique d'animation, vous devez donc cocher "Prise de vue réelle".**
- `Plateforme(s)` : Pour les kara de jeux vidéo, sur quelle(s) plateforme(s) le jeu est-il sorti ?
- `Genres` : Quel genre d'oeuvre est-ce ?
- `Origine(s)` : D'où vient le karaoké ?
- `Divers` : Le tag "Long" est réservé aux karaoké qui durent 5 minutes ou plus, en général.
- `Groupe(s)` : Permet de regrouper plusieurs kara dans des "familles". Même en ne rentrant rien, KM ajoutera automatiquement un tag indiquant la décennie d'où vient votre karaoké.
- `Warning(s)` : Permet d'indiquer que ce karaoké peut potentiellement poser des problèmes à un public. Par exemple epilepsie, spoilers ou pour adultes.

## Validation et création du fichier kara

Une fois que tous les champs sont remplis, il ne vous reste plus qu'à cliquer sur le gros bouton bleu ![Formulaire](../img/form/form29.png) pour générer le fichier kara adéquat.

Si tout s'est bien passé, vous devriez voir apparaitre le bandeau suivant en haut de la page :

![Formulaire](../img/form/form44.png)

Le fichier kara que vous venez de créer a été placé dans votre dossier primaire `karaokes`, et la vidéo et le fichier *.ass* ont été renommés selon ce que vous avez indiqué dans le formulaire, et placés respectivement dans les dossiers `medias` et `lyrics` (ou dans le premier d'entre eux si vous en avez indiqué plusieurs dans votre fichier `config.yml`).

Pour les retrouver parmi tous les autres fichiers, triez-les par date de modification dans votre explorateur de fichiers.

Aussi, votre nouveau karaoké a été ajouté à votre base de données sans que vous ayez à régénérer celle-ci, ce qui vous permet de tester tout de suite votre ajout.

Si vous souhaitez modifier un fichier kara, [rendez-vous sur cette page.](editkaraoke.md#modifier-les-informations-dun-karaoke-ou-dune-serie)

## Pro-tips de remplissage des champs

Cette section concerne principalement les règles et autres exceptions de la base karaokés officielle de Karaoke Mugen.

### Références

{!contrib-guide/inc_series-names.md!}

### Typographie

Des petits détails sur le contenu de certains champs :

#### Titres du morceau

**Tous** les symboles / caractères spéciaux sont tolérés. N'hésitez donc pas à utiliser les noms originaux des chansons, avec des 2 points, des points d'exclamation, des cœurs, des étoiles, etc. ( ex : "Van!shment Th!s World" ; "Sparkle☆彡Star☆Twinkle Pretty Cure" ; "shØut")

#### Chanteur(s) et compositeur(s)

**[Utilisez de préférence anidb](https://anidb.net/perl-bin/animedb.pl?show=song&songid=63520)** et indiquez le nom de l'artiste *tel qu'il est retranscrit officiellement*. Si aucun artiste ne vous est suggéré par l'application, c'est qu'il n'est pas dans la base de données à l'instant T (ou que vous l'avez mal écrit). S'il n'existe pas, continuez de taper son nom et validez avec `Ajout`. L'artiste sera ainsi crée lorsque vous aurez terminé et validé le formulaire. Comme dis plus haut, veillez à bien écrire le nom. Il serait tentant d'écrire par exemple **_Être_** avec un e majuscule, comme le reste du nom de groupe, mais l'écriture officielle est bien [Black Raison d'être](https://anidb.net/creator/38790) (cf la ligne *"official name"*).

On a parfois le cas d'un groupe avec plusieurs personnalités (exemple : le groupe [FictionJunction](https://en.wikipedia.org/wiki/FictionJunction) de Yuki Kajiura, [Black Raison d'être](https://anidb.net/creator/38790) pour la série Chûnibyô) ou personnages qui chantent (à peu près tous les animes d'idol), dans ce cas, il faut indiquer **à la fois le nom du groupe et le nom des chanteurs / chanteuses.**

Dans le cas où ce sont les personnages d'une série qui chantent, on indiquera **le nom des comédiens de doublage.**

#### Créateur(s)

La liste de suggestions est déjà bien complète, mais si le nom que vous souhaitez ajouter n'est pas dans la liste déroulante, aidez-vous de [la section "Studios / Créateurs" de la page Références](../references) listant la majorité des studios d'animation / de jeux vidéo / de toku / de films live, avec leur nom **officiel**.

### Règles

Dès lors qu'une chanson est liée à une oeuvre, on renseigne l'oeuvre (dans le champ `série` donc), même si c'est une reprise qui n'est pas audible dans l'oeuvre en question.

**Lorsque vous ajoutez :**

- Un titre de chanson
- Une nouvelle personne (en tant que chanteur ou compositeur)
- Une nouvelle série

Prenez en compte ces quelques notes sur la typographie des noms en japonais :

{!contrib-guide/inc_japanese.md!}

Ces règles doivent être **obligatoirement** appliquées pour les champs `Titre de la chanson` (**_Shinzô wo Sasageyo_**), `Série(s)` (**_Chûnibyô_**), `Chanteur(s)` (**_Mâya Uchida_**), `Compositeur(s)` (**_Yôko Kanno_**).

### Règles particulières

En restant dans le cas où vous ajoutez une entrée qui n'existe pas encore dans la base de données, **toujours** écrire `Prénom` puis `Nom`. Pour ne pas se tromper, anidb indique toujours les artistes en `Nom` `Prénom`, il suffit donc d'inverser. MyAnimeList fait la même chose sur ses fiches.
