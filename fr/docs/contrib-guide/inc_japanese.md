Vous l'avez peut-être remarqué, certains noms sont accentués, par exemple l'artiste **_Mâya Uchida_**.

En effet, là aussi dans un souci d'uniformisation, il a été décidé des règles d'écriture pour les noms

- Un nom avec un "o" long (ou) devient "ô".
	- Il arrive que certains noms soient écrit avec des "oh", comme "Yuusha-Oh Tanjou". Il s'agit de la transcription hepburn anglophone, qui correspond en fait à un "ou" en hepburn modifié. *"Oh"* est donc égal à *"ô"* là aussi.
- Un nom avec un double "o" (oo) **reste** "oo".
- Un nom avec un "u" long (uu) devient "û".
- Un nom avec un "a" long (aa) devient "â".
- Un nom avec un "e" long (ee) devient "ê".
- Un nom avec un "i" long (ii) **reste** "ii".
- Un nom avec une apostrophe (Shin'ichi par exemple), garde son apostrophe.