![Bannière Karaoke Mugen, dessin de Sedeto](./img/banner.webp)

# Accueil

Bienvenue dans la documentation de [**Karaoke Mugen**](https://mugen.karaokes.moe/).

**Karaoke Mugen** est un logiciel de gestion de karaokés, utilisable lors d'évènements (conventions, livestreams *Twitch*, soirées *Discord*) ou en famille. Cette documentation parcourt plusieurs sujets, tous disponibles dans la barre latérale de navigation. Vous voudrez peut-être commencer par :

- [Installer le logiciel](./user-guide/install.md)
- [Contribuer à la base de karaokés](./contrib-guide/base.md)