# Karaoke Mugen documentation

This is the repository for Karaoke Mugen's documentation.

It generates [this part of the site](http://docs.karaokes.moe)

## Install

To test it out you'll need [MkDocs](http://www.mkdocs.org) and the [ReadTheDocs theme](https://github.com/mkdocs/mkdocs/tree/master/mkdocs/themes/readthedocs). We provide a `requirements.txt` and `Pipfile`.

In short:

```sh
pip install -U -r requirements.txt
# -- or --
pipenv install
```

## Testing

Once installed, you can use `mkdocs serve` (don't forget to do `pipenv shell` before if you use pipenv) in `fr` or `en` folders to spin up a local webserver to see your changes.